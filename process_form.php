<?php
$host = "localhost";
$username = "root";
$password = "";
$database = "kapp";

try {
    $conn = new mysqli($host, $username, $password, $database);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
} catch (Exception $e) {
    die("Error connecting to the database: " . $e->getMessage());
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
   
    $name = $_POST["name"];
    $email = $_POST["email"];

  
    $uploadDir = __DIR__ . "/uploads/";
    $resumeName = basename($_FILES["resume"]["name"]);
    $resumePath = $uploadDir . $resumeName;

    if (move_uploaded_file($_FILES["resume"]["tmp_name"], $resumePath)) {
        $insertQuery = "INSERT INTO submissions (`name`, email, resume_path) VALUES (?, ?, ?)";

        $stmt = $conn->prepare($insertQuery);
        $stmt->bind_param("sss", $name, $email, $resumePath);


       
        // echo "Query: $insertQuery<br>";
        // echo "Parameters: name=$name, email=$email, resumePath=$resumePath<br>";

        try {
            if ($stmt->execute()) {
                echo "Thank you, $name! Your information has been submitted.<br>";
                echo "Resume: <a href='$resumePath' target='_blank'>$resumeName</a>";
            } else {
                echo "Error executing query: " . $stmt->error;
            }
        } catch (Exception $e) {
            echo "Exception during query execution: " . $e->getMessage();
        }

        $stmt->close();
    } else {      
        echo "Sorry, there was an error uploading your resume.";
    }
} else {
    
    echo "Form not submitted.";
}

$conn->commit();
?>
